package com.sample.lambda.typeInference;

public class LambdaTypeInference

{
    public static void  main(String args[])
{
        StringLengthLambda stringLengthLambda = (s) -> s.length();
        AddLambdaFunction addLambdaFunction = ( a, b) ->  a + b;
        SafeDivideLambdaFunction safeDivideLambdaFunction = (a, b) -> {
            if(b==0) return 0;
            return a/b;
        };

    System.out.println("String Length: " + stringLengthLambda.StringLength("Happy"));
    System.out.println("Sum of Two Numbers 20 and 30: " + addLambdaFunction.addNumbers(20, 30));
    System.out.println("Safe Divide: 10/2 = " + safeDivideLambdaFunction.safeDivide(10, 2));
    System.out.println("Safe Divide: 10/0 = " + safeDivideLambdaFunction.safeDivide(10, 0));

}

interface StringLengthLambda{
        public int StringLength(String s);
}

interface  AddLambdaFunction{
        public int addNumbers(int a, int b);
}

interface SafeDivideLambdaFunction{
        public int safeDivide(int no1, int no2);
}

}
