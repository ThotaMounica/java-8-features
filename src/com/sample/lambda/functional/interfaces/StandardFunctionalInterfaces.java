package com.sample.lambda.functional.interfaces;

import com.sample.lambda.unit1.Person;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StandardFunctionalInterfaces {

    public static void main(String args[]) {

        List<Person> people = Arrays.asList(
                new Person("Harry", "Potter", 20),
                new Person("Ron", "Weasley", 20),
                new Person("Hermoine", "Granger", 20),
                new Person("Ginny", "Weasley", 19)
        );

        System.out.println("Normal List:" + people);

        //Sort the List by Last Name
        Comparator<Person> lastNameComparator = ( p1, p2) -> p1.getLastName().compareTo(p2.getLastName());

        people.sort(lastNameComparator);

        System.out.println("List after sorting with lastName: " + people);

        //Create a method that prints all elements in the list
        System.out.println("Print all Names: ");
        performConditionally(people, (p)-> true, p -> System.out.println(p));

        //Create a method that prints all people that have last Name Beginning with W

        System.out.println("Print lastName starts with W: ");
        performConditionally(people, p-> p.getLastName().startsWith("W"), p -> System.out.println(p.getLastName()));

        //Create a method that prints all people that have firstName starts with H

        System.out.println("Print FirstName starts with H : ");
        performConditionally(people, p-> p.getFirstName().startsWith("H"), p-> System.out.println(p.getFirstName()));

    }


    private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer)
    {
        for(Person p: people)
        {
            if(predicate.test(p))
            {
                consumer.accept(p);
            }
        }
    }
}
