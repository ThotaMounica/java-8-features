package com.sample.lambda.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Unit1ExerciseLambdaSolution {

    public static void main(String args[]) {

        List<Person> people = Arrays.asList(
                new Person("Harry", "Potter", 20),
                new Person("Ron", "Weasley", 20),
                new Person("Hermoine", "Granger", 20),
                new Person("Ginny", "Weasley", 19)
        );

        System.out.println("Normal List:" + people);

        //Sort the List by Last Name
        Comparator<Person> lastNameComparator = ( p1, p2) -> p1.getLastName().compareTo(p2.getLastName());

        people.sort(lastNameComparator);

        System.out.println("List after sorting with lastName: " + people);

        //Create a method that prints all elements in the list
        System.out.println("Print all Names: ");
        printElementsConditionally(people, (p)-> true);

        //Create a method that prints all people that have last Name Beginning with W

        System.out.println("Print lastName starts with W: ");
        printElementsConditionally(people, p-> p.getLastName().startsWith("W"));

        //Create a method that prints all people that have firstName starts with H

        System.out.println("Print FirstName starts with H : ");
        printElementsConditionally(people, p-> p.getFirstName().startsWith("H"));

    }


    private static void printElementsConditionally(List<Person> people, Predicate<Person> predicate)
    {
        for(Person p: people)
        {
            if(predicate.test(p))
            {
                System.out.println(p);
            }
        }
    }
}
