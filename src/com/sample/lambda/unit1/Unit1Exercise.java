package com.sample.lambda.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Unit1Exercise {

    public static void main(String args[]) {

        List<Person> people = Arrays.asList(
                new Person("Harry", "Potter", 20),
                new Person("Ron", "Weasley", 20),
                new Person("Hermoine", "Granger", 20),
                new Person("Ginny", "Weasley", 19)
        );

        System.out.println("Normal List:" + people);

        //Sort the List by Last Name
        Comparator<Person> lastNameComparator = new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getLastName().compareTo(p2.getLastName());
            }
        };

        people.sort(lastNameComparator);
        System.out.println("Sorted List:" + people);

        //Create a method that prints all elements in the list

        Printer printPeople = new Printer() {
            @Override
            public void print(List<Person> people) {
                System.out.println("Print all People: ");
                for (Person p : people) {
                    System.out.println(p);
                }
            }
        };
        printElements(people, printPeople);

        //Create a method that prints all people that have last Name Beginning with W

        Printer specialPrintPeople = new Printer() {
            @Override
            public void print(List<Person> people) {
                System.out.println("Print people with lastName starting W:");
                for (Person p : people) {
                    if (p.getLastName().startsWith("W") || p.getLastName().startsWith("w")) {
                        System.out.println(p);
                    }
                }
            }
        };

        printElements(people, specialPrintPeople);

    }

    private static void printElements(List<Person> people, Printer p) {
        p.print(people);
    }

    interface Printer {
        public void print(List<Person> people);
    }


}
