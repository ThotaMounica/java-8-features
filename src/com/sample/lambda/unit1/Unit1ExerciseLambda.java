package com.sample.lambda.unit1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Unit1ExerciseLambda {

    public static void main(String args[]) {

        List<Person> people = Arrays.asList(
                new Person("Harry", "Potter", 20),
                new Person("Ron", "Weasley", 20),
                new Person("Hermoine", "Granger", 20),
                new Person("Ginny", "Weasley", 19)
        );

        System.out.println("Normal List:" + people);

        //Sort the List by Last Name
        Comparator<Person> lastNameComparator = (Person p1, Person p2) -> p1.getLastName().compareTo(p2.getLastName());

        people.sort(lastNameComparator);

        System.out.println("List after sorting with lastName: " + people);

        //Create a method that prints all elements in the list
        Printer normalPrinter = (peopleList) -> {
            System.out.println("Printing all the elements in the list: ");
            for (Person p : peopleList) {
                System.out.println(p);
            }
        };

        printElements(people, normalPrinter);

        //Create a method that prints all people that have last Name Beginning with W
        Printer specialPrinter = (people1) -> {
            System.out.println("Print the person with lastName starting with W");
            for (Person p : people1) {
                if (p.getLastName().startsWith("W") || p.getLastName().startsWith("w")) {
                    System.out.println(p);
                }
            }
        };
        printElements(people, specialPrinter);
    }


    private static void printElements(List<Person> people, Printer p) {
        p.print(people);
    }

    interface Printer {
        public void print(List<Person> people);
    }
}
