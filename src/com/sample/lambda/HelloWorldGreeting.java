package com.sample.lambda;

public class HelloWorldGreeting implements Greeting{
    @Override
    public void perform() {
        System.out.println("Hello world! from Class which implements an interface..");
    }
}
