package com.sample.lambda;

@FunctionalInterface
public interface Greeting {

    public void perform();

}
