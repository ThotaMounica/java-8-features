package com.sample.lambda.runnable;

public class RunnableExample {

    public static void main(String args[])
    {
        Thread thread1 = new Thread(){
            @Override
            public void run() {
                System.out.println("I am running inside a runnable interface anonymous inner class");
            }
        };
        thread1.run();

        Thread thread2 = new Thread(()-> System.out.println("I am running inside lambda expression"));
        thread2.run();

    }
}
