package com.sample.lambda;

public class Greeter
{
    public static void greet(Greeting greeting){
        greeting.perform();
    }

    public static void main(String[] args) {

        Greeter greeter = new Greeter();

        Greeting helloWorldGreeting = new HelloWorldGreeting();

        greeter.greet(helloWorldGreeting);

        Greeting lambdaGreeting = () -> System.out.println("Hello World! from lambda Expression...");
        greeter.greet(lambdaGreeting);

        Greeting anonymousGreeting = new Greeting() {
            @Override
            public void perform() {
                System.out.println("Hello world! from an Anonymous Interface...");
            }
        };
        greeter.greet(anonymousGreeting);

        Runnable runnableInterface = () -> System.out.println("Hello world! from runnable Interface...");
        runnableInterface.run();

    }
}