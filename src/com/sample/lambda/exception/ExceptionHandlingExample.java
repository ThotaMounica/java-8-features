package com.sample.lambda.exception;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public class ExceptionHandlingExample {

    public static void main(String args[])
    {

        List listOfNumbers = Arrays.asList(1, 2, 3, 4, 5);
        int key = 0;

        perform(listOfNumbers, key, (n, k) -> System.out.println(n+k));

        perform(listOfNumbers, key, wrapperLambda((n, k) -> System.out.println(n/k)));
    }

    private static BiConsumer<Integer, Integer> wrapperLambda(BiConsumer<Integer, Integer> consumer) {

        return (a, b) -> {
            try{
                consumer.accept(a, b);
            }
            catch(ArithmeticException e){
                System.out.println("ArithmeticException Occurred....");
            }
        };
    }

    public static void perform(List<Integer> listOfNumbers, int key, BiConsumer<Integer, Integer> consumer)
    {
        for(int n: listOfNumbers){
            consumer.accept(n, key);
        }

    }

}
