Java -8 Concepts from https://www.youtube.com/playlist?list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3


### NOTES
With java-8 you could have methods implemented in the interface itself.
**For example:** you could have an interface with 3 methods, out of which 2 have default implementation and
one method is the abstract method. So, that can be a functional interface.


An interface which has only one abstract interface is called functional interface.

The @FunctionalInterface annotation is completely optional.
The Java compiler does not require if for your lambda types. But it is a good practice to add it.
